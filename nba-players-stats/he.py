#! /usr/bin/python

import re

hs = dict()

duplicates = 0

dat = open("player_data.csv", "r")
dat.readline()

l = dat.readline()[:-1]

while l:
    n_l = l.split(',')

    name = n_l[0]
    height = n_l[-1]

    if name in hs:
        duplicates += 1
    else:
        hs[name] = height
    
    l = dat.readline()[:-1]

dat.close()

print(duplicates)

new = 0

dat = open("Players.csv", "r")
dat.readline()

l = dat.readline()[:-1]

while l:
    n_l = l.split(',')

    name = n_l[1]
    height = n_l[2]

    if name not in hs:
        hs[name] = height
        new += 1
    
    l = dat.readline()[:-1]

dat.close()

print(new)

empty = 0

ss = open("Seasons_Stats.csv", "r")
ss_h = open("ss_heights.csv", "w")

l = ss.readline()[:-1]
l += ",height"
ss_h.write(l)

l = ss.readline()[:-1]
while l:
    name = re.findall("^[^,]*,[^,]*,[^,]*", l)[0].split(',')[-1]
    
    l = "\n" + l + ","
    
    if name not in hs:
        empty += 1
    else:
        l = l + hs[name]
    
    ss_h.write(l)
    
    l = ss.readline()[:-1]

ss.close()
ss_h.close()

print(empty)

